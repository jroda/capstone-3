import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import './index.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { ProductContextProvider } from './context/ProductContext';
import { AuthContextProvider } from './context/AuthContext';
import { CartContextProvider } from './context/CartContext';
import { OrderContextProvider } from './context/OrderContext';

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <AuthContextProvider>
      <CartContextProvider>
        <ProductContextProvider>
          <OrderContextProvider>
            <App />
          </OrderContextProvider>
        </ProductContextProvider>
      </CartContextProvider>
    </AuthContextProvider>
  </React.StrictMode>
);
