import React, { useEffect, useState } from 'react';
import { Button, Col, Container, Form, Row } from 'react-bootstrap';
import { useProduct } from '../hooks/useProduct';

const EditProduct = ({ product }) => {
  const [images, setImages] = useState([]);
  const [hasUploaded, setHasUploaded] = useState(false);
  const [subCat, setSubCat] = useState('');

  const { updateProduct } = useProduct();

  const [formData, setFormData] = useState(product);

  const handleChange = (e) => {
    const { name, value } = e.target;

    setFormData((prevFormData) => {
      return {
        ...prevFormData,
        [name]: value,
      };
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log(formData);
    updateProduct(
      formData.productId,
      formData.detailsId,
      formData.name,
      formData.description,
      formData.color,
      formData.size,
      formData.stocks,
      formData.price
    );
  };

  return (
    <Container className="p-3">
      <Row className="mt-3">
        <Col
          md={6}
          className="d-flex flex-column justify-content-center align-items-center border rounded py-3"
        >
          <img
            src={product.img_url[0] || product.img_url}
            width={350}
            height={350}
          />
        </Col>

        <Col md={6} className="px-5">
          <Form onSubmit={handleSubmit}>
            <Form.Label>Name</Form.Label>
            <Form.Control
              type="text"
              name="name"
              placeholder="Enter name"
              onChange={handleChange}
              value={formData.name}
              required
            />

            <Form.Label className="mt-2">Description</Form.Label>
            <Form.Control
              type="text"
              name="description"
              placeholder="Enter description"
              onChange={handleChange}
              value={formData.description}
              required
            />

            <Form.Group className="d-flex justify-content-between mt-2">
              <Col md={5}>
                <Form.Label>Color</Form.Label>
                <Form.Control
                  type="text"
                  name="color"
                  placeholder="Enter color"
                  onChange={handleChange}
                  value={formData.color}
                  required
                />
              </Col>

              <Col md={5}>
                <Form.Label>Size</Form.Label>
                <Form.Control
                  type="text"
                  name="size"
                  placeholder="Enter size"
                  onChange={handleChange}
                  value={formData.size}
                  required
                />
              </Col>
            </Form.Group>

            <Form.Group className="d-flex justify-content-between mt-2">
              <Col md={5}>
                <Form.Label>Stocks</Form.Label>
                <Form.Control
                  type="number"
                  name="stocks"
                  placeholder="Enter stocks"
                  onChange={handleChange}
                  value={formData.stocks}
                  required
                />
                <Form.Text className="text-muted d-block">
                  Stocks should not be less than 1
                </Form.Text>
              </Col>

              <Col md={5}>
                <Form.Label>Price</Form.Label>
                <Form.Control
                  type="number"
                  name="price"
                  placeholder="Enter price"
                  onChange={handleChange}
                  value={formData.price}
                  required
                />
              </Col>
            </Form.Group>

            {/* <Form.Label>Main Category</Form.Label>
            <Form.Select
              name="main"
              onChange={handleChange}
              value={formData.main}
            >
              <option>Men's Headwear</option>
              <option>Women's Headwear</option>
              <option>Unisex Headwear</option>
              <option>Men's Apparel</option>
              <option>Women's Apparel</option>
              <option>Men's Footwear</option>
              <option>Women's Footwear</option>
              <option>Unisex Footwear</option>
            </Form.Select>

            <Form.Label className="mt-2">Sub Category</Form.Label>
            <Form.Control
              type="text"
              name="sub"
              placeholder="Ex: Short, Slipper, Sandals"
              onChange={(e) => setSubCat(e.target.value)}
              value={subCat}
              required
            /> */}
            <Button
              type="submit"
              className="mt-2 col-12 mx-auto bg-success border-success"
            >
              Update
            </Button>
          </Form>
        </Col>
      </Row>
    </Container>
  );
};

export default EditProduct;
