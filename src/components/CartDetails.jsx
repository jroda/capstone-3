import { faTrashCan } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
import { Button } from 'react-bootstrap';
import { useCart } from '../hooks/useCart';

const CartDetails = ({ item }) => {
  const { removeCartItem } = useCart();
  return (
    <tbody>
      <tr>
        <td>
          <img
            src="https://i.pinimg.com/564x/c1/1d/16/c11d164de692594acf53c9a855093139.jpg"
            width={50}
            height={50}
          />
        </td>
        <td>{item.name}</td>
        <td>{item.color}</td>
        <td>{item.size}</td>
        <td>{item.quantity}</td>
        <td>&#8369; {item.price}</td>
        <td>&#8369; {item.subTotal}</td>
        <td>
          <Button
            variant="danger"
            className="border-0"
            onClick={() => removeCartItem(item._id)}
          >
            <FontAwesomeIcon icon={faTrashCan} />
          </Button>
        </td>
      </tr>
    </tbody>
  );
};

export default CartDetails;
