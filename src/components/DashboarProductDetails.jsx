import React, { useState } from 'react';
import { Button, Modal, Table } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { useProduct } from '../hooks/useProduct';
import EditProduct from './EditProduct';

const DashboarProductDetails = ({ product }) => {
  const { archivedUnarchived } = useProduct();

  const [showChoose, setShowChoose] = useState(false);
  const [choosenProduct, setChoosenProduct] = useState(null);
  const handleCloseChoose = () => setShowChoose(false);
  const handleShowChoose = () => setShowChoose(true);

  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = (prod) => {
    setChoosenProduct(prod);
    setShow(true);
  };

  const priceSet = new Set();
  let priceRange = [];

  if (product) {
    for (let i = 0; i < product.details.length; i++) {
      priceSet.add(product.details[i].price);
    }
    priceRange = Array.from(priceSet);
  }

  return (
    <tbody>
      <tr>
        <td>
          <Link to={`../product/${product._id}`}>
            <img
              src={product.img_url[0] || product.img_url}
              width={50}
              height={50}
            />
          </Link>
        </td>
        <td>{product.name}</td>
        <td>{product.description}</td>
        {priceRange.length < 2 ? (
          <td>&#8369;{priceRange[0]}</td>
        ) : (
          <td>
            &#8369;{priceRange[0]} - &#8369;
            {priceRange[priceRange.length - 1]}
          </td>
        )}

        <td className={product.isActive ? 'text-success' : 'text-danger'}>
          {product.isActive ? 'ACTIVE' : 'INACTIVE'}
        </td>
        <td>
          <Button
            className={`${
              product.isActive ? 'bg-danger' : 'bg-success'
            } border-0`}
            onClick={() => archivedUnarchived(product._id)}
          >
            {product.isActive ? 'ARCHIVED' : 'UNARCHIVED'}
          </Button>
          <Button onClick={handleShowChoose} className="bg-primary">
            Edit
          </Button>
        </td>
      </tr>
      <Modal show={showChoose} onHide={handleCloseChoose} fullscreen={true}>
        <Modal.Header closeButton>
          <Modal.Title className="text-center w-100">
            CHOOSE PRODUCT TO EDIT
          </Modal.Title>
        </Modal.Header>
        <Table className="text-center mt-3">
          <thead>
            <tr>
              <th>Image</th>
              <th>Product Name</th>
              <th>Color</th>
              <th>Size</th>
              <th>Stocks</th>
              <th>Price</th>
              <th>Action</th>
            </tr>
          </thead>
          {product.details.map((detail) => (
            <tbody>
              <tr>
                <td>
                  <Link to={`../product/${product._id}`}>
                    <img
                      src={product.img_url[0] || product.img_url}
                      width={50}
                      height={50}
                    />
                  </Link>
                </td>
                <td>{product.name}</td>
                <td>{detail.color}</td>
                <td>{detail.size}</td>
                <td>{detail.stocks}</td>
                <td>{detail.price}</td>
                <td>
                  <Button
                    onClick={() =>
                      handleShow({
                        productId: product._id,
                        name: product.name,
                        img_url: product.img_url,
                        detailsId: detail._id,
                        description: product.description,
                        color: detail.color,
                        size: detail.size,
                        stocks: detail.stocks,
                        price: detail.price,
                      })
                    }
                    className="bg-primary"
                  >
                    Edit
                  </Button>
                </td>
              </tr>
            </tbody>
          ))}
          {/* <ChooseProduct key={product._id} product={product} /> */}
        </Table>
      </Modal>
      <Modal show={show} onHide={handleClose} fullscreen={true}>
        <Modal.Header closeButton>
          <Modal.Title className="text-center w-100">EDIT PRODUCT</Modal.Title>
        </Modal.Header>
        <EditProduct key={product._id} product={choosenProduct} />
      </Modal>
    </tbody>
  );
};

export default DashboarProductDetails;
