import React from 'react';
import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom';
import { useAuthContext } from './hooks/useAuthContext';
import Home from './pages/Home';
import Login from './pages/Login';
import Signup from './pages/Signup';
import ShowProduct from './pages/ShowProduct';
import Cart from './pages/Cart';
import AppNavbar from './components/AppNavbar';
import Dashboard from './pages/Dashboard';
import Page404 from './pages/Page404';
import Order from './pages/Order';
import Profile from './pages/Profile';

function App() {
  const { user } = useAuthContext();
  return (
    <div className="App">
      <BrowserRouter>
        <AppNavbar />
        <div className="pages">
          <Routes>
            <Route path="/" element={<Home />} />

            <Route
              path="/login"
              element={!user ? <Login /> : <Navigate to="/" />}
            />

            <Route
              path="/signup"
              element={!user ? <Signup /> : <Navigate to="/" />}
            />

            <Route path="/cart" element={<Cart />} />

            <Route path="/dashboard" element={<Dashboard />} />

            <Route path="/orders" element={<Order />} />

            <Route path="/profile" element={<Profile />} />

            <Route path="/product/:productId" element={<ShowProduct />} />

            <Route path="*" element={<Page404 />} />
          </Routes>
        </div>
      </BrowserRouter>
    </div>
  );
}

export default App;
