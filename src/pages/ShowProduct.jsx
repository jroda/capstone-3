import React, { useEffect, useState } from 'react';
import { Button, Carousel, Col, Container, Row } from 'react-bootstrap';
import { useParams, Link } from 'react-router-dom';
import { uid } from 'uid';
import { useAuthContext } from '../hooks/useAuthContext';
import { useCart } from '../hooks/useCart';
import { useCartContext } from '../hooks/useCartContext';
import { useProductsContext } from '../hooks/useProductsContext';

const ShowProduct = () => {
  const { productId } = useParams();
  const { buyNow } = useCart();
  const { singleProduct, dispatch: productDispatch } = useProductsContext();
  const { addCart, error, user } = useCart();
  const [product, setProduct] = useState('');
  const [isLoading, setIsLoading] = useState(true);
  const [quantity, setQuantity] = useState(1);
  const [size, setSize] = useState('');
  const [color, setColor] = useState('');
  const [stock, setStock] = useState(0);
  const [price, setPrice] = useState(null);
  const [detailsId, setDetailsId] = useState(null);
  const [isEmpty, setIsEmpty] = useState(true);

  const priceSet = new Set();
  let priceRange = [];
  const sizesWithColors = [];
  const colors = [];

  useEffect(() => {
    if (color && size && stock) {
      setIsEmpty(false);
    } else {
      setIsEmpty(true);
    }
  }, [size, color, stock]);

  useEffect(() => {
    const fetchProduct = async () => {
      const response = await fetch(
        `${import.meta.env.VITE_LOCALHOST_API}/api/products/${productId}`
      );
      const json = await response.json();

      if (!response.ok) {
        setError(json.error);
        setIsLoading(false);
      }

      if (response.ok) {
        // setProduct(json);
        productDispatch({ type: 'SET_SINGLE_PRODUCT', payload: json });
        setIsLoading(false);
      }
    };

    fetchProduct();
  }, []);

  singleProduct &&
    singleProduct.details.map((detail) => {
      sizesWithColors.push({ ...detail });
      !colors.includes(detail.color) && colors.push(detail.color);
    });

  if (singleProduct) {
    for (let i = 0; i < singleProduct.details.length; i++) {
      priceSet.add(singleProduct.details[i].price);
    }
  }
  priceRange = Array.from(priceSet).sort(function (a, b) {
    return a - b;
  });

  const addQuantity = () => {
    setQuantity((prevQuantity) => prevQuantity + 1);
  };

  const subtractQuantity = () => {
    if (quantity > 1) setQuantity((prevQuantity) => prevQuantity - 1);
  };

  const [index, setIndex] = useState(0);

  const handleSelect = (selectedIndex, e) => {
    setIndex(selectedIndex);
  };

  useEffect(() => {
    for (let i = 0; i < sizesWithColors.length; i++) {
      if (sizesWithColors[i].color === color) {
        if (sizesWithColors[i].size === size) {
          setPrice(sizesWithColors[i].price);
          setDetailsId(sizesWithColors[i]._id);
        }
      }
    }
  }, [color, size]);

  return (
    <Container className="bg-dark p-5 min-vh-75 my-3 rounded">
      {!isLoading && (
        <Row className="bg-light rounded">
          <Carousel
            as={Col}
            md={6}
            activeIndex={index}
            onSelect={handleSelect}
            slide={true}
            interval={null}
            className="max-vh-75 bg-warning"
          >
            {singleProduct.img_url.map((prod) => (
              <Carousel.Item key={prod}>
                <img className="d-block w-100" src={prod} />
              </Carousel.Item>
            ))}
          </Carousel>

          <Col md={6} className="p-5">
            <h3>{singleProduct.name}</h3>
            {price ? (
              <h5>&#8369;{price}</h5>
            ) : priceRange.length < 2 ? (
              <h5>&#8369;{priceRange[0]}</h5>
            ) : (
              <h5>
                &#8369;{priceRange[0]} - &#8369;
                {priceRange[priceRange.length - 1]}
              </h5>
            )}
            <p>{singleProduct.description}</p>

            <h6 className="mr-2">Colors: </h6>
            {colors.map((col) => (
              <Button
                // variant="outline-dark"
                key={uid()}
                className="rounded mx-1"
                onClick={() => {
                  setColor(col);
                }}
              >
                {col}
              </Button>
            ))}

            <h6 className="mt-3 mr-2">Sizes: </h6>
            {sizesWithColors.map(
              (swc) =>
                swc.color === color && (
                  <Button
                    key={uid()}
                    className="mx-1 rounded"
                    onClick={(e) => {
                      setSize(swc.size);
                      setStock(swc.stocks);
                    }}
                    disabled={!swc.hasStocks}
                  >
                    {swc.size}
                  </Button>
                )
            )}

            <h6 className="mt-3 mr-2">Stocks: </h6>
            {sizesWithColors.map(
              (swc) =>
                swc.color === color &&
                swc.size === size && <span key={uid()}>{swc.stocks}</span>
            )}

            <h6 className="mt-3 mr-2">Quantity: </h6>
            <Button onClick={subtractQuantity}>-</Button>
            <input
              type="number"
              className="form-control input-quantity text-center mx-1 d-inline-block"
              onChange={(e) =>
                e.target.value <= stock
                  ? setQuantity(Number(e.target.value))
                  : setQuantity(100)
              }
              value={quantity || setQuantity(1)}
              // disabled={true}
            />
            <Button
              className="btn btn-primary"
              onClick={addQuantity}
              disabled={quantity === stock}
            >
              +
            </Button>

            {user && user.isAdmin ? (
              <Row className="justify-content-center mt-5">
                <Button
                  variant="success"
                  as={Link}
                  to="/dashboard"
                  className="d-inline-block w-25 mx-2"
                >
                  Edit
                </Button>
              </Row>
            ) : (
              <Row className="justify-content-center mt-5">
                <Button
                  variant="danger"
                  as={!user && Link}
                  to={!user ? '/login' : ''}
                  className="d-inline-block w-25 mx-2"
                  disabled={isEmpty}
                  onClick={
                    user
                      ? () =>
                          buyNow(
                            productId,
                            color,
                            size,
                            quantity,
                            price,
                            detailsId
                          )
                      : ''
                  }
                >
                  Buy Now!
                </Button>
                <Button
                  variant="primary"
                  as={!user && Link}
                  to={!user ? '/login' : ''}
                  className="d-inline-block w-25 mx-2"
                  onClick={
                    user ? () => addCart(productId, quantity, color, size) : ''
                  }
                  disabled={isEmpty}
                >
                  Add to cart
                </Button>
              </Row>
            )}
          </Col>
        </Row>
      )}
    </Container>
  );
};

export default ShowProduct;
