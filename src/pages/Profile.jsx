import React from 'react';
import { Col, Container, Row, Stack } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import { useAuthContext } from '../hooks/useAuthContext';

const Profile = () => {
  const { user } = useAuthContext();
  return !user ? (
    <Navigate to="/" />
  ) : (
    <Container className="min-vh-75 my-3 p-5">
      <Row className="align-items-center justify-content-center">
        <Col>
          {user.gender === 'male' ? (
            <img
              src="/images/users-icon/male.svg"
              className="drop-down-icon me-2"
              alt="male-profile"
            />
          ) : (
            <img src="/images/users-icon/female.svg" alt="female-profile" />
          )}
          <h5>Name</h5>
          <h5>Cellphone Number</h5>
          <h5>Gender</h5>
          <h5>Address</h5>
          <h5>Username</h5>
          <h5>Email</h5>
          <h5>Admin</h5>
        </Col>
      </Row>
    </Container>
  );
};

export default Profile;
