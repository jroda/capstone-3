import React, { Fragment, useEffect, useState } from 'react';
import { Button, Col, Container, Row, Table } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import OrderDetails from '../components/OrderDetails';
import { useAuthContext } from '../hooks/useAuthContext';
import { useOrder } from '../hooks/useOrder';
import { useOrderContext } from '../hooks/useOrderContext';

const Order = () => {
  const [error, setError] = useState(null);
  let { orders, dispatch: orderDispatch } = useOrderContext();
  const [orderContainer, setOrderContainer] = useState(null);
  const { orderReceived, approvedOrder, cancelOrder } = useOrder();
  const { user, token } = useAuthContext();

  useEffect(() => {
    const fetchOrderRegular = async () => {
      const response = await fetch(
        `${import.meta.env.VITE_LOCALHOST_API}/api/users/orders`,
        {
          headers: { Authorization: `Bearer ${token}` },
        }
      );

      const json = await response.json();

      if (!response.ok) {
        console.log(json.error);
        setError(json.error);
      }

      if (response.ok) {
        setOrderContainer(json);
        orderDispatch({ type: 'SET_ORDERS', payload: json });
      }
    };

    const fetchOrderAdmin = async () => {
      const response = await fetch(
        `${import.meta.env.VITE_LOCALHOST_API}/api/users/showAllOrders`,
        {
          headers: { Authorization: `Bearer ${token}` },
        }
      );

      const json = await response.json();

      if (!response.ok) {
        console.log(json.error);
        setError(json.error);
      }

      if (response.ok) {
        setOrderContainer(json);
        orderDispatch({ type: 'SET_ORDERS', payload: json });
      }
    };
    user && user.isAdmin && fetchOrderAdmin();
    user && !user.isAdmin && fetchOrderRegular();
  }, [user]);

  const showAll = () => {
    if (orderContainer) {
      orderDispatch({ type: 'SET_ORDERS', payload: orderContainer });
    }
  };

  const showPending = () => {
    if (orderContainer) {
      showAll();
      orderDispatch({ type: 'PENDING' });
    }
  };

  const showApproved = () => {
    if (orderContainer) {
      showAll();
      orderDispatch({ type: 'APPROVED' });
    }
  };

  const showCancelled = () => {
    if (orderContainer) {
      showAll();
      orderDispatch({ type: 'CANCELLED' });
    }
  };

  const showReceived = () => {
    if (orderContainer) {
      showAll();
      orderDispatch({ type: 'RECEIVED' });
    }
  };

  const showToReceived = () => {
    if (orderContainer) {
      showAll();
      orderDispatch({ type: 'TO_RECEIVED' });
    }
  };
  return !token ? (
    <Navigate to="/" />
  ) : (
    user && (
      <Container className="py-3 bg-light my-5">
        <h1 className="display-6 text-center">Orders</h1>
        {user.isAdmin && (
          <div className="mb-3 w-100 d-flex justify-content-center align-items-center">
            <Button className="mx-1" onClick={showAll}>
              All
            </Button>
            <Button variant="warning" className="mx-1" onClick={showPending}>
              Pending
            </Button>
            <Button variant="info" className="mx-1" onClick={showApproved}>
              Approved
            </Button>
            <Button variant="danger" className="mx-1" onClick={showCancelled}>
              Cancelled
            </Button>
            <Button variant="success" className="mx-1" onClick={showReceived}>
              Received by buyer
            </Button>
          </div>
        )}

        {!user.isAdmin && (
          <div className="mb-3 w-100 d-flex justify-content-center align-items-center">
            <Button className="mx-1" onClick={showAll}>
              All
            </Button>
            <Button variant="warning" className="mx-1" onClick={showPending}>
              Pending
            </Button>
            <Button variant="danger" className="mx-1" onClick={showCancelled}>
              Cancelled
            </Button>
            <Button variant="info" className="mx-1" onClick={showToReceived}>
              To Receive
            </Button>
            <Button variant="success" className="mx-1" onClick={showReceived}>
              Received
            </Button>
          </div>
        )}
        <Row className="justify-content-center align-items-center bg-light">
          <Col>
            {orders.length > 0 ? (
              <Fragment>
                {orders.map((order) => {
                  return (
                    <Table
                      key={order._id}
                      // border="success"
                      bordered
                      striped
                      className={`mb-5 text-center ${
                        (order.status === 'approved' &&
                          order.orderReceived &&
                          'border-success') ||
                        (order.status === 'pending' && 'border-warning') ||
                        (order.status === 'approved' && 'border-info') ||
                        (order.status === 'cancelled' && 'border-danger')
                      }`}
                    >
                      <thead className="">
                        <tr>
                          <th colSpan={7}>ID: {order._id}</th>
                        </tr>
                        <tr>
                          <th>Image</th>
                          <th>Product Name</th>
                          <th>Color</th>
                          <th>Size</th>
                          <th>Quantity</th>
                          <th>Price</th>
                          <th>Sub-Total</th>
                        </tr>
                      </thead>
                      <tbody>
                        {order.products.map((product) => {
                          return (
                            <OrderDetails key={product._id} product={product} />
                          );
                        })}
                      </tbody>
                      <tfoot className="fw-bold">
                        <tr>
                          <td colSpan={6} className="text-start ps-5">
                            Total
                          </td>
                          <td>&#8369; {order.totalAmount}</td>
                        </tr>
                        <tr>
                          <td colSpan={7} className="text-center">
                            <p className="p-0">Order Status: {order.status}</p>
                            {user && order.orderReceived && (
                              <p className="p-0 text-success">
                                Already Received
                              </p>
                            )}
                            {user &&
                              !user.isAdmin &&
                              order.status === 'cancelled' && (
                                <p className="p-0 text-danger">
                                  Order Cancelled
                                </p>
                              )}

                            {user && user.isAdmin ? (
                              <Button
                                variant="outline-success"
                                className={`text-light bg-success ${
                                  order.status === 'approved' ||
                                  order.status === 'cancelled'
                                    ? 'd-none'
                                    : ''
                                }`}
                                onClick={() => approvedOrder(order._id)}
                                disabled={order.status === 'approved'}
                              >
                                Accept Order
                              </Button>
                            ) : order.status === 'pending' ? (
                              <Button
                                variant="outline-danger"
                                className={`text-light bg-danger ${
                                  order.orderReceived ? 'd-none' : ''
                                }`}
                                onClick={() => cancelOrder(order._id)}
                              >
                                Cancel Order
                              </Button>
                            ) : (
                              order.status === 'approved' && (
                                <Button
                                  variant="outline-success"
                                  className={`text-light bg-success ${
                                    order.orderReceived ? 'd-none' : ''
                                  }`}
                                  onClick={() => orderReceived(order._id)}
                                >
                                  Order Received
                                </Button>
                              )
                            )}
                          </td>
                        </tr>
                      </tfoot>
                    </Table>
                  );
                })}
              </Fragment>
            ) : (
              <div>{error}</div>
            )}
          </Col>
        </Row>
      </Container>
    )
  );
};

export default Order;
