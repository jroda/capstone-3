import Swal from 'sweetalert2';
import { useAuthContext } from './useAuthContext';
import { useOrderContext } from './useOrderContext';

export const useOrder = () => {
  const { user, token } = useAuthContext();
  const { dispatch: orderDispatch } = useOrderContext();

  const orderReceived = async (orderId) => {
    console.log(orderId);
    const result = await Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, already received it!',
    });

    if (result.isConfirmed) {
      const response = await fetch(
        `${import.meta.env.VITE_LOCALHOST_API}/api/users/orderReceived`,
        {
          method: 'POST',
          body: JSON.stringify({ orderId }),
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          },
        }
      );

      const json = await response.json();

      if (!response.ok) {
        Swal.fire('Somethings not right!', `${json.error}`, 'error');
      }

      if (response.ok) {
        Swal.fire('Success!', `Order received`, 'success');
        orderDispatch({ type: 'RECEIVED_ORDERS', payload: orderId });
      }
    }
  };

  const approvedOrder = async (orderId) => {
    const result = await Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, approved it!',
    });

    if (result.isConfirmed) {
      const response = await fetch(
        `${import.meta.env.VITE_LOCALHOST_API}/api/users/approvedOrder`,
        {
          method: 'POST',
          body: JSON.stringify({ orderId }),
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          },
        }
      );

      const json = await response.json();

      if (!response.ok) {
        Swal.fire('Somethings not right!', `${json.error}`, 'error');
      }

      if (response.ok) {
        Swal.fire('Success!', `Order has been Approved`, 'success');
        orderDispatch({ type: 'APPROVED_ORDERS', payload: orderId });
      }
    }
  };

  const cancelOrder = async (orderId) => {
    const result = await Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, cancel it!',
    });

    if (result.isConfirmed) {
      const response = await fetch(
        `${import.meta.env.VITE_LOCALHOST_API}/api/users/cancel`,
        {
          method: 'POST',
          body: JSON.stringify({ orderId }),
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          },
        }
      );

      const json = await response.json();

      if (!response.ok) {
        Swal.fire('Somethings not right!', `${json.error}`, 'error');
      }

      if (response.ok) {
        Swal.fire('Success!', `Order has been cancelled`, 'success');
        orderDispatch({ type: 'CANCELLED_ORDERS', payload: orderId });
      }
    }
  };

  return { orderReceived, approvedOrder, cancelOrder };
};
