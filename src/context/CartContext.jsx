import React, { createContext, useEffect, useReducer, useState } from 'react';
import PropTypes from 'prop-types';
import { useAuthContext } from '../hooks/useAuthContext';

export const CartContext = createContext();

export const cartReducer = (state, action) => {
  switch (action.type) {
    case 'GET_CARTS':
      return { items: action.payload };
    case 'ADD_CART':
      if (state.items.length > 0) {
        const { productId, color, size, quantity, subTotal } =
          action.payload[action.payload.length - 1];

        for (let i = 0; i < state.items.length; i++) {
          if (
            state.items[i].productId === productId &&
            state.items[i].color === color &&
            state.items[i].size === size
          ) {
            state.items[i].quantity = quantity;
            state.items[i].subTotal = subTotal;

            return state;
          }
        }
      }
      return {
        items: [action.payload[action.payload.length - 1], ...state.items],
      };
    case 'DELETE_CART':
      return {
        items: state.items.filter((item) => item._id !== action.payload),
      };
    case 'CHECKOUT':
      return {
        items: [],
      };
    default:
      return state;
  }
};

export const CartContextProvider = ({ children }) => {
  const { user, token } = useAuthContext();
  const [error, setError] = useState(null);
  const [state, dispatch] = useReducer(cartReducer, {
    items: [],
  });

  useEffect(() => {
    const fetchCart = async () => {
      try {
        const response = await fetch(
          `${import.meta.env.VITE_LOCALHOST_API}/api/users/showCart`,
          {
            headers: { Authorization: `Bearer ${token}` },
          }
        );
        const json = await response.json();
        if (!response.ok) {
          setError(json);
        }

        if (response.ok) {
          dispatch({
            type: 'GET_CARTS',
            payload: json.products || json[0].products,
          });
        }
      } catch (error) {
        setError(error);
      }
    };
    user && !user.isAdmin && fetchCart();
  }, [user, dispatch]);

  return (
    <CartContext.Provider value={{ ...state, dispatch, error, setError }}>
      {children}
    </CartContext.Provider>
  );
};

CartContextProvider.propTypes = {
  children: PropTypes.object,
};
